# Xml2Ui

DroidScript plugin to build an interface from a μXML file describing it.

# IMPORTANT!

Development is being temporarily halted. SPK file available for those who want to check out what's currently available.

---



### How to use

Using a text editor, create a μXML file. (We'll use "example.xml" for these examples.)

1. Start with the root tag, `ui`.

```xml
<ui>

</ui>
```

2. Add a layout. 

```xml
<ui>
  <layout>

  </layout>
</ui>
```

3. And last, add a text box.

```xml
<ui>
  <layout>
    <text>Hello, world!</text>
  </layout>
</ui>
```

### Behind the scenes

1. The tag `ui` starts the stack.

2. As each `layout` tag is processed, it gets added to the stack.

3. As each child tag (in this example a `text` tag) is processed,
it gets added to the stack.

4. When the closing tag `/text` is reached, the text control is added to the layout.

5. When the closing `/layout` tag is reached, the layout is added to the app.

6. Finally, when the closing `/ui` tag is reached, the ui is written to a temporary file and then ran.

### Tag class

```js
ControlTag
{
  constructor( id, command, arguments )  
  {
    
    this.command = command;
    this.arguments = arguments;
  }
}
```